const openCity = (e) => {
    const tabContents = document.querySelectorAll('.tab-content');
  
    for (const tabContent of tabContents) {
        tabContent.classList.remove('tab-content--show');
    }
  
    const tabs = document.querySelectorAll('.tab');
  
    for (const tab of tabs) {
        tab.classList.remove('tab--activate');
    }
  
    document.getElementById(e.target.dataset.contentId).classList.add('tab-content--show');
    e.target.classList.add('tab--activate');
}

document.getElementById('tab1').addEventListener('click', (e) => openCity(e));

document.getElementById('tab2').addEventListener('click', (e) => openCity(e));

document.getElementById('tab3').addEventListener('click', (e) => openCity(e));
