const control = document.getElementById('control');
const optionGroup = document.getElementById('option-group');
const searchField = document.getElementById('search-field');

document.getElementById('modal').addEventListener('click', e => {
        if (e.target.classList.contains('modal')) control.open = false;
});

document.getElementById('option-group').addEventListener('click', e => {
        if (e.target.classList.contains('option')) {
            control.open = false;
            control.children[0].textContent = e.target.textContent;
        }
});
/*console.log(control.children);*/

/**
   * Execute the search.
   */
searchField.addEventListener('change', e => {

    const searchText = searchField.value.trim();
    
    // Verify that the value is not a empty string.
    // Generally when the search box is cleared.
    if (!searchText) {
        // idealmente, poner los resultados en el estado por defecto.
        console.log(`El valor es invalido: ${searchText}. No se busca.`);

        return;
    }

    // do search using fetch api, if no result are found, show no items.
    console.log(`El valor es valido: ${searchText}.`);

    /* fetch(`http://127.0.0.1:8000/ventas/get-services-data?term=${searchText}`)
        .then(response => response.ok ? Promise.resolve(response): Promise.reject(response))
        .then(response => response.json())
        .then(response => {
        
            // clean the list.
            // list.replaceChildren();

            const fragment = document.createDocumentFragment();

            for (const item of response) {
                const li = document.createElement('LI');
                li.classList.add('element-list__item');
                li.setAttribute('data-id', item.id);
                li.textContent = item.name;

                fragment.appendChild(li);
            }


            list.appendChild(fragment);
        })
        .catch(); */
});
