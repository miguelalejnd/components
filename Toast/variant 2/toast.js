const init = () => {
  const node = document.createElement('section')
  node.classList.add('gui-toast-group')

  document.firstElementChild.insertBefore(node, document.body)
  return node
}

const createToast = text => {
  const node = document.createElement('output')
  
  node.innerText = text
  node.classList.add('gui-toast')
  //node.setAttribute('role', 'status')
  //node.setAttribute('aria-live', 'polite')

  return node
}

const addToast = toast => {
  /* 
  Esta línea utiliza la desestructuración de objetos en JavaScript para extraer
  la propiedad matches del objeto retornado por window.matchMedia(). matches es
  un booleano que indica si la media query proporcionada coincide con las
  características del dispositivo actual. La desestructuración renombra esta
  propiedad extraída como motionOK. Por lo tanto, motionOK contendrá el valor
  de matches.
  */
  const { matches:motionOK } = window.matchMedia(
    '(prefers-reduced-motion: no-preference)'
  )
  
  // Si la logitud es mayor a cero se evalua como true
  Toaster.children.length && motionOK
    ? flipToast(toast)
    : Toaster.appendChild(toast)
}

const Toast = text => {
  let toast = createToast(text)
  addToast(toast)

  return new Promise(async (resolve, reject) => {
    await Promise.allSettled(
      toast.getAnimations().map(animation => 
        animation.finished
      )
    )
    Toaster.removeChild(toast)
    resolve() 
  })
}

// https://aerotwist.com/blog/flip-your-animations/
const flipToast = toast => {
  // FIRST
  const first = Toaster.offsetHeight

  // add new child to change container size
  Toaster.appendChild(toast)

  // LAST
  const last = Toaster.offsetHeight

  // INVERT
  const invert = last - first

  // PLAY
  const animation = Toaster.animate([
    { transform: `translateY(${invert}px)` },
    { transform: 'translateY(0)' }
  ], {
    duration: 150,
    easing: 'ease-out',
  })

  animation.startTime = document.timeline.currentTime
}

// Se ejecuta la funcion init al cargar el módulo.
const Toaster = init()

// Exportar unicamente la funcion Toast.
export default Toast
