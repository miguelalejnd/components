const showToast = () => {
  const toast = document.getElementById('toast');
  
  toast.classList.add('toast--show');
  
  setTimeout(function(){ toast.classList.toggle('toast--show'); }, 3000);
}

document.getElementById('tbutton').addEventListener('click', showToast);
