const control = document.getElementById('modal-btn');
const closeButton = document.getElementById('close-button');
const optionGroup = document.getElementById('option-group');
const searchField = document.getElementById('search-field');
const dialog = document.getElementById('modal');

/*
document.getElementById('option-group').addEventListener('click', e => {
        if (e.target.classList.contains('option')) {
            control.children[0].textContent = e.target.textContent;
        }
});
console.log(control.children);*/


// "Show the dialog" button opens the dialog modally
control.addEventListener("click", () => {
  dialog.showModal();
});

// "Show the dialog" button opens the dialog modally
closeButton.addEventListener("click", () => {
  dialog.close();
});

/* "Close" button closes the dialog
control.addEventListener("click", () => {
  dialog.close();
});*/


/**
   * Execute the search.
   */
searchField.addEventListener('change', e => {

    const searchText = searchField.value.trim();
    
    // Verify that the value is not a empty string.
    // Generally when the search box is cleared.
    if (!searchText) {
        // idealmente, poner los resultados en el estado por defecto.
        console.log(`El valor es invalido: ${searchText}. No se busca.`);

        return;
    }

    // do search using fetch api, if no result are found, show no items.
    console.log(`El valor es valido: ${searchText}.`);

    /* fetch(`http://127.0.0.1:8000/ventas/get-services-data?term=${searchText}`)
        .then(response => response.ok ? Promise.resolve(response): Promise.reject(response))
        .then(response => response.json())
        .then(response => {
        
            // clean the list.
            // list.replaceChildren();

            const fragment = document.createDocumentFragment();

            for (const item of response) {
                const li = document.createElement('LI');
                li.classList.add('element-list__item');
                li.setAttribute('data-id', item.id);
                li.textContent = item.name;

                fragment.appendChild(li);
            }


            list.appendChild(fragment);
        })
        .catch(); */
});
