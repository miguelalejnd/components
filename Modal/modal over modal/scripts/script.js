const firstModal = document.getElementById('first-dialog');
const secondModal = document.getElementById('second-dialog');

// the button open the first dialog modally
document.getElementById('first-dialog-open-btn').addEventListener("click", () => {
  firstModal.showModal();
});

// the button close the first dialog modally.
document.getElementById('first-dialog-close-btn').addEventListener("click", () => {
  firstModal.close();
});

// This button open the second dialog modally.
document.getElementById('second-dialog-open-btn').addEventListener("click", () => {
  secondModal.showModal();
});

// the button close the second dialog modally.
document.getElementById('second-dialog-close-btn').addEventListener("click", () => {
  secondModal.close();
});
