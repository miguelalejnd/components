const button = document.getElementById('modalButton');
const modal = document.getElementById('modal');

button.addEventListener('click', event => modal.classList.add('modal__show'));

modal.addEventListener('click', event => {
    if (event.target.classList.contains('modal__show')) modal.classList.remove('modal__show');
});
