const sidenav = document.getElementById('menu-open')
const closenav = document.getElementById('menu-close')
const opennav = document.getElementById('menu-button')

/**
  * set focus to our open/close buttons after animation
  */
  
opennav.addEventListener('click', e => {
    sidenav.classList.add('menu__open')
    closenav.focus()
})

// sidenav.addEventListener('transitionend', e => closenav.focus())

closenav.addEventListener('click', e => sidenav.classList.remove('menu__open'))

/**
  * close our menu when esc is pressed. this is optional but good for accesibility.
  */
sidenav.addEventListener('keyup', e => {
  if (e.code === 'Escape')
      sidenav.classList.remove('menu__open')
})
