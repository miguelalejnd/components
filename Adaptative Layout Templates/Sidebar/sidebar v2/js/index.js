const sidenav = document.getElementById('sidenav-open')
const closenav = document.getElementById('sidenav-close')
const opennav = document.getElementById('sidenav-button')

/**
  * set focus to our open/close buttons after animation
  */
  
opennav.addEventListener('click', e => {
    sidenav.classList.add('sidenav__open')
    closenav.focus()
})

// sidenav.addEventListener('transitionend', e => closenav.focus())

closenav.addEventListener('click', e => sidenav.classList.remove('sidenav__open'))

/**
  * close our menu when esc is pressed. this is optional but good for accesibility.
  */
sidenav.addEventListener('keyup', e => {
  if (e.code === 'Escape')
      sidenav.classList.remove('sidenav__open')
})
