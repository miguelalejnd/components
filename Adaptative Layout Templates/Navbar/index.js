const navToggle = document.querySelector(".layout-button");
const navMenu = document.querySelector(".nav-menu");

navToggle.addEventListener("click", () => {
  navMenu.classList.toggle("nav-menu_visible");

  // if (navMenu.classList.contains("nav-menu_visible")) {
  //   navToggle.setAttribute("aria-label", "Cerrar menú");
  // } else {
  //   navToggle.setAttribute("aria-label", "Abrir menú");
  // }
});

// close our menu when esc is pressed
// navToggle.addEventListener('keyup', e => {
//   if (e.code === 'Escape')
//     navMenu.classList.remove("nav-menu_visible");
// })
